﻿// footboll_table.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "ctime"

using namespace std;

void analyze_table(int** table, int n);
void consider_balls(int** table, int* sum_gols, int n);
void consider_places(int** table, int* sum_gols, int* wins_loses, int n);
void edit_table(int** table, int** table2, int n);
void fill_table(int** table, int n);
void print_table(int** table, int n);


int main()
{
	int n;
	srand(time(NULL));
	cout << "Enter n: ";
	cin >> n;
	int** table = new int*[n];
	for (int i = 0; i < n; i++)
		table[i] = new int[n];
	fill_table(table, n);
	analyze_table(table, n);

	int** table2 = new int*[n];
	for (int i = 0; i < n; i++)
		table2[i] = new int[n];

	for (int i = 0; i < n; i++) 
		for (int j = 0; j < n; j++) 
			table2[i][j] = table[i][j];
	cout << endl;
	print_table(table, n);
	edit_table(table, table2, n);
	print_table(table, n);

}

void analyze_table(int** table, int n) {  // функция для определения мест у команд
	int* places = new int[n];
	int* wins_loses = new int[n];
	int* sum_gols = new int[n];
	for (int i = 0; i < n; i++) { // заполнение массивов, где будут храниться:
		places[i] = 0;			  // места команд
		sum_gols[i] = 0;		  // забитые/пропущенные мячи
		wins_loses[i] = 0;		  // кол-во побед команды
	}

	consider_balls(table, sum_gols, n);
	consider_places(table, sum_gols, wins_loses, n);

	int max = -10000;
	int k = 0;
	int sum = -1000;

	for (int i = 0; i < n; i++) {
		max = -1000;
		for (int j = 0; j < n; j++) {
			if (wins_loses[j] > max) {
				max = wins_loses[j];
				k = j;
				
			}
		}
		//cout << max << " nt  " << k << endl;
		places[i] = k;
		wins_loses[k] = -10000;
		k = 0;
	}
	for (int i = 0; i < n; i++)
		table[places[i]][places[i]] = i + 1;
}

void consider_balls(int** table, int* sum_gols, int n) { // функция суммы мячей
	for (int i = 0; i < n; i++) {						 // по всей таблице кроме главной диагонале
		for (int j = 0; j < n; j++) {
			if (i != j) {
				sum_gols[i] += table[i][j];
				sum_gols[i] -= table[j][i];
			}
		}
	}
}

void consider_places(int** table, int* sum_gols, int* wins_loses, int n) { 
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (i != j) {
				if (table[i][j] > table[j][i]) { 
					wins_loses[i] += 1;

				}
				else if (table[i][j] == table[j][i]) { // проверка на равное число побед 
					if (sum_gols[i] > sum_gols[j]) {
						wins_loses[i] += 1;
					}
					else {
						wins_loses[i] -= 1;
					}
				}
				else {
					wins_loses[i] -= 1;
				}
			}
		}
	}
}

void edit_table(int** table, int** table2, int n) { // функция для перестановки команд от первого места к последнему
	int* places = new int[n];
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (table2[j][j] == i+1) {
				places[i] = j;
			}
		}
	}
	cout << endl;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			table[i][j] = table2[places[i]][places[j]];
		}
	}
}

void fill_table(int** table, int n) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (i != j) {
				table[i][j] = rand() % 10;
			}
			else
				table[i][j] = 0;
		}
	}
}

void print_table(int** table, int n) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cout << table[i][j] << " ";
		}
		cout << endl;
	}
}
