﻿// ConsoleApplication5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "string"
#include "fstream"

using namespace std;

char sum_long_num(char* num1, char* num2, char* rez) {
	//int count = strlen(num1);
	int len = strlen(num2);
	int ostatok = 0;
	int sum = 0;
	int n1, n2;
	rez[0] = '0';
	for (int i = strlen(num1) - 1; i >= 0; i--) {
		n1 = int(num1[i]) - 48;

		n2 = int(num2[i]) - 48;

		sum = n1 + n2 + ostatok;
		if (sum >= 10) {
			rez[i + 1] = char((sum % 10) + 48);
			ostatok = 1;
		}
		else {
			rez[i + 1] = char(sum + 48);
			ostatok = 0;
		}
	}

	if (ostatok != 1) {
		for (int i = 0; i < strlen(rez); i++) {
			rez[i] = rez[i + 1];
		}
		rez[strlen(rez)] = '\0';
	}
	else
		rez[0] = '1';
	return*rez;
}

bool read_line(ifstream& file, char* buffer, int count) {


	buffer[0] = 0; // Для последующей проверки
	file.getline(buffer, count, '\n');
	if (file.fail())
	{
		if (buffer[0] > 0)
			cout << "Слишком длинная строка во входном файле" << endl;
		return false;
	}
	return true;
}

void print_numbers(char* num1, char* num2, char* rez) {
	cout << endl;
	for (int i = 0; i < strlen(num1); i++)
		cout << num1[i];
	cout << endl;
	for (int i = 0; i < strlen(num2) - 1; i++)
		cout << num2[i];
	cout << endl;
	for (int i = 0; i < strlen(rez); i++) {
		cout << rez[i];
	}
}

int main()
{
	ifstream input("input.txt");
	ofstream output("output.txt");
	char buf[31];
	read_line(input, buf, 31);
	int count = int(buf);
	char* num1 = new char[count];
	char* num3 = new char[count];
	char* num2 = new char[count];

	read_line(input, num1, count);
	read_line(input, num3, count);
	int l = strlen(num1) - strlen(num3);
	for (int i = 0; i < l; i++)
		num2[i] = '0';
	int i = 0;
	for (i; i < strlen(num3); i++)
		num2[l + i] = num3[i];
	num2[l + i + 1] = '\0';
	char* rez = new char[strlen(num1) + 1];
	rez[strlen(num1) + 1] = '\0';
	for (int i = 0; i < strlen(rez); i++) {
		rez[i] = '1';
	}

	sum_long_num(num1, num2, rez);
	print_numbers(num1, num2, rez);
	output << rez << endl;
	return 0;

}




