﻿// moon_data.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//


#include "pch.h"
#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <sstream>
#include <chrono>
#include <ctime>




using namespace std;

typedef struct {
	int date;
	int time;
	double el;
} time_data;


double get_el(char* str, int len);
int change_seeek(char* buf, int ofset);
int count_days(int month_start, int month_end);
string format_date(string user_date);
void search_date(ifstream& file, int user_date, time_data* data_arr);
int input_date();
string reverse_string(string first_string, int ln);
string format_time(int incorrect_time);
void format_output(int start_moon, int end_moon, int max_moon);
void fast_result(ifstream& file, int user_date);
void fast_search2_0(ifstream& fil, int user_date);

#define _CRT_SECURE_NO_WARNINGS
int main()
{	
	time_data* data_arr = new time_data[1440];
	while (true)
	{
		ifstream fin;
		int user_date = input_date();
		int year = (user_date / 10000);
		string file_name = "moon";
		file_name = file_name + to_string(year) + ".dat";
		fin.open(file_name, std::ios::in | std::ios::binary);
		cout << user_date << endl;

		if (fin.fail()){
			cout << "Unknown year" << endl;
			continue;
		}	
		
		
		//search_date(fin, user_date, data_arr);
		//fin.seekg(0);
		
		fast_search2_0(fin, user_date);
		//search_date(fin, user_date, data_arr);

	}
	
}

void fast_search2_0(ifstream& fil, int user_date) {
	long long int t1 = clock();
	time_data dat;

	dat.date = 0;
	dat.el = -90;
	dat.time = 0;

	int start_moon = 90;
	int end_moon = -90;
	int time_max_moon_pos = 0;

	int count = 0;
	int size = fil.seekg(0, ios::end).tellg();
	fil.seekg(0);
	char * buf = new char[size + 1];
	fil.read(buf, size);
	buf[size] = '\0';
	long long int t3 = clock();
	long long int t4 = t3-t1;
	cout << float(t4) / CLOCKS_PER_SEC << " seconds for reading" << endl;
	string rubbish, moon_data;
	double check_var;
	double max_moon_position = -90;

	int ofset = 25;
	char char_user_date[9];
	_itoa(user_date, char_user_date, 10);
	char_user_date[8] = '\0';

	while (dat.date < user_date + 1) {
		if (dat.date == user_date) {
			if ((check_var <= 0) && (dat.el >= 0)) {
				start_moon = dat.time;

			}
			if ((check_var >= 0) && (dat.el <= 0))
				end_moon = dat.time;

			if (dat.el > max_moon_position) {
				max_moon_position = dat.el;
				time_max_moon_pos = dat.time;
			}
		}
		check_var = dat.el;
		char date[9];
		
		char* bufpt = buf;
		bufpt += ofset;
		strncpy(date, bufpt, 8);
		date[8] = '\0';
		dat.date = atoi(date);
		//cout << date << char_user_date << endl;
		if (dat.date == user_date) {
			//cout << date << endl;
			ofset += 8;
			ofset += 1;
			char time[7];
			bufpt += 9;
			strncpy(time, bufpt, 6);
			ofset += 6;
			bufpt += 6;
			time[6] = '\0';
			dat.time = atoi(time);
			ofset += 36 - 15;
			char el[11];
			bufpt += 36 - 15;
			strncpy(el, bufpt, 10);
			el[10] = '\0';
			dat.el = get_el(el, 10);
			ofset += 10;
		}

		ofset = change_seeek(buf, ofset);
		if (buf[ofset] == '\0')
			break;
		
		//char rub[1];
		//do
		//{
		//	rub[0] = buf[ofset];
		//	ofset++;
		//	moon_data = moon_data + rub[0];
		//} while (rub[0] != '\n');
	}
	if ((start_moon == 90) && (end_moon == -90)) {
		cout << "Unknown date" << endl;
		return;
	}
	long long int t2 = clock();
	long long int t = t2 - t1;
	format_output(start_moon, end_moon, time_max_moon_pos);
	cout << float(t) / CLOCKS_PER_SEC << " seconds" << endl;
}

int change_seeek(char* buf, int ofset) {
	while (buf[ofset] != '\n') {
		if (buf[ofset] == '\0')
			return ofset;
		ofset++;
	}
	return ++ofset;
}

double get_el(char* str, int len) {
	char pred_simv = str[0];
	char rez[10];
	int cnt = 0;
	rez[9] = '\n';
	int i;
	for (i = 0; i < len; i++) {
		if (str[i] != ' ') {
			rez[cnt] = str[i];
			cnt++;
		}
		if ((pred_simv <= '9') && (pred_simv >= '0') && ((str[i] >= '9' ) || (str[i] <= '0')) && (str[i] != '.')) {
			return atof(rez);
		}

		pred_simv = str[i];
	}
}


int input_date() {
	string user_date;
	smatch rtx;
	regex temp("[0-9]{2}\\.[0-9]{2}\\.[1-2]{1}[0-9]{1}[0-9]{2}");
	regex temp_alt("[0-9]{4}[1-2]{1}[0-9]{1}[0-9]{2}");
	int rez_data = 0;
	while (true)
	{
		cout << "Enter your date (01122017 or 01.12.2017): ";
		cin >> user_date;
		if (user_date == "exit")
			exit(0);
		if (regex_match(user_date, rtx, temp_alt)) {
			rez_data = stoi(user_date);
		}
		else if (regex_match(user_date, rtx, temp)) {
			user_date = format_date(user_date);
			rez_data = stoi(user_date);
		}
		else
			cout << "Incorrect date, try again" << endl;

		if (rez_data) {
			int month = (rez_data / 10000) % 100;

			if (month > 12) {
				cout << "Unknown month" << endl;
			}
			else {
				int day = rez_data / 1000000;
				int year = rez_data % 10000;
				rez_data = (year * 10000) + (month * 100) + day;
				return rez_data;

			}
		}
	}
}

string format_date(string user_date) {
	string result = "";
	for (int i = 0; i < 10; i++) {
		if (user_date[i] != '.') {
			result = result + user_date[i];
		}
	}
	return result;
}

void format_output(int start_moon, int end_moon, int max_moon) {
	
	string time_start, time_end, time_max;
	time_start = format_time(start_moon);
	time_end = format_time(end_moon);
	time_max = format_time(max_moon);
	cout << "Start moon: " << time_start << endl;
	cout << "End moon: " << time_end << endl;
	cout << "Climax of the moon:" << time_max << endl;
}

string format_time(int incorrect_time) {
	int num = 0;
	int buffer_num = incorrect_time;
	string buffer;
	for (int i = 0; i < 6; i++) {
		num = buffer_num % 10;
		buffer += to_string(num);
		buffer_num /= 10;
	}
	string result = reverse_string(buffer, 6);
	return result;
}

string reverse_string(string first_string, int ln) {
	string last_string = "000000";
	for (int i = 0, j = ln-1; i < j; i++) {
		last_string[j - i] = first_string[i];
	}
	return last_string;
}


int count_days(int month_start, int month_end) {
	int count_days = 0;
	int arr[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	for (int i = month_start-1; i < month_end-1; i++) {
		count_days += arr[i];
	}
	return count_days;
}