﻿// vector_summ.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "fstream"
#include "string"

using namespace std;



typedef struct {
	int x = 0;
	int y = 0;
} pnt;

void change_arr(pnt* data, int len, int k);
int consider_vector(int x1, int y1, int x2, int y2);

int main()
{
	ifstream fin("Granitsy_uchastka2.txt");
	int len;
	char a[3];
	fin >> len;
	pnt* data = new pnt[len];
	for (int i = 0; i < len; i++) {
		fin >> data[i].x;
		fin >> data[i].y;
	}
	int min = data[0].x;
	int k = 0;
	for (int i = 0; i < len; i++) {
		if (data[i].x < min) {
			min = data[i].x;
			k = i;
		}
	}
	cout << k << endl;
	change_arr(data, len, k);
	k = 0;
	cout << data[0].x << endl;
	//for (int i = 0; i < len; i++) {
	//	data[i].x -= data[0].x;
	//	data[i].y -= data[0].y;
	//}
	double S = 0;
	cout << data[0].x << endl;
	for (int i = 1; i < len; i++) {
		S += consider_vector((data[i - 1].x-data[0].x), data[i - 1].y - data[0].y, data[i].x - data[0].x, data[i].y - data[0].y);
	}
	S = fabs(S) / 1000000 /2.0;
	cout << S << endl;
}

void change_arr(pnt* data, int len, int k) {
	while (k != 0) {
		pnt d = data[0];
		for (int i = 1; i < len; i++) {
			data[i - 1] = data[i];
		}
		data[len - 1] = d;
		k--;
	}
}

int consider_vector(int x1, int y1, int x2, int y2) {
	int k = ((x1*y2) - (x2*y1));
	return k;
}