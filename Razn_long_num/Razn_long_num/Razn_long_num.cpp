﻿// Razn_long_num.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "fstream"

using namespace std;
int razn_long_num(int* num1, int* num2, int count);
void char_to_int(int* num, char* ch_num);
void edit_num(char* num2, char* num3, int count, int l);
void print_arr(int* num2_int, int count);
int del_null(int* num1_int);
void int_to_char(int* num, char* rez, int j, int count);

int main()
{
	ifstream input("input.txt");
	ofstream output("output.txt");
	int count = 0;
	input >> count;
	char* num1 = new char[count+1]; // первое число
	char* num3 = new char[count+1]; // второе число
	char* num2 = new char[count+1]; // здесь будет второе число, с добавленными нулями 
	int* num1_int = new int[count];
	int* num2_int = new int[count];

	input >> num1;
	input >> num3;
	int l = strlen(num1) - strlen(num3);

	edit_num(num2, num3, count, l);
	
	char_to_int(num1_int, num1);
	char_to_int(num2_int, num2);

	print_arr(num1_int, count);
	print_arr(num2_int, count);
	razn_long_num(num1_int, num2_int, count);

	int j = del_null(num1_int);
	char* rez = new char[count - j+1];

	int_to_char(num1_int, rez, j, count);
	cout << "Answer: ";
	cout << rez << endl;
	output << rez << endl;
}

int del_null(int* num1_int) {
	int j = 0;
	while (num1_int[j] == 0)
		j++;
	return j;
}

void edit_num(char* num2, char* num3, int count, int l) {
	for (int i = 0; i < l; i++)
		num2[i] = '0';

	for (int i = l; i < count; i++)
		num2[i] = num3[i - l];
	num2[count] = '\0';
}

void char_to_int(int* num, char* ch_num) {
	for (int i = 0; i < strlen(ch_num); i++)
		num[i] = int(ch_num[i]) - 48;
}

void int_to_char(int* num, char* rez, int j, int count) {
	int y = 0;
	for (int g = j; g < count; g++) {
		rez[y] = char(num[g] + 48);
		y++;
	}
}

void print_arr(int* num2_int, int count) {
	for (int i = 0; i < count; i++)
		cout << num2_int[i];
	cout << endl;
}

int razn_long_num(int* num1, int* num2, int count) {
	int n = 0;
	int ost = 0;
	int k = 0;
	for (int i = count - 1; i >= 0; i--) {
		n = num1[i] - num2[i];
		if (n < 0) {
			k++;
			while (num1[i + k] == 0)
				k++;
			while (k != 0) {
				num1[i - k] -= 1;
				num1[i - k + 1] += 10;
				k--;
			}
			n = num1[i] - num2[i];
		}
		num1[i] = n;
		
	}
	for (int i = 0; i < count; i++)
		cout << num1[i];
	cout << endl;
	return 0;	
}