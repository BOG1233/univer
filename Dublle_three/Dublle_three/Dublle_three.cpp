﻿// Dublle_three.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

using namespace std;

int ten_to_three(int num, int* arr_num);
void print_arr(int* arr, int len);
long long dublle_num(int* arr, int len);
long long three_to_ten(long long num, int len);


int main()
{
	int* arr = new int[70];
	cout << "Enter your num: ";
	int input;
	cin >> input;
	cout << endl;
	int len = ten_to_three(input, arr);
	print_arr(arr, len);
	cout << len << endl;
	long long num = dublle_num(arr, len);
	long long result = three_to_ten(num, len * 2);
	cout << "Result: " << result << endl;
}

int ten_to_three(int num, int* arr_num) {
	int ost = 0;
	int n = num;
	int i = 0;
	while (n > 2) {
		ost = n % 3;
		arr_num[i] = ost;
		n /= 3;
		i++;
	}
	arr_num[i] = n;
	i++;
	return i;
}

void print_arr(int* arr, int len) {
	for (int i = 0; i < len; i++)
		cout << arr[i];
	cout << endl;
}

long long dublle_num(int* arr, int len) {
	long long num = 0;
	int k = 1;
	for (int i = 0; i < len; i++) {
		num += arr[i] * k;
		k *= 10;
		num += arr[i] * k;
		k *= 10;
	}
	return num;
}
long long three_to_ten(long long num, int len) {
	int k = 1;
	int sign = 0;
	long long result = 0;
	for (int i = 0; i < len; i++) {
		sign = num % 10;
		result += sign * k;
		k *= 3;
		num /= 10;
	}
	return result;
}